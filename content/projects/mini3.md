+++
title = "Autonomous Vehicle Motion Forecasting"
description = "https://github.com/zaxiang/Autonomous-vehicle-motion-forecasting"
+++

# Project Overview

Developed a Seq2Seq (Encoder-Decoder) RNN model with LSTM to predict vehicle movement.
