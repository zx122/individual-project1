+++
title = "Suicide & Economic Factors"
description = "https://github.com/zaxiang/DDS_Project"
+++

Diversity in Data Science @UCSD Project

# Project Overview.
- The goal of the project is to use statistical tests and machine learning models to check which socioeconomic and mental health factors have significant impact on suicide rate.
- Employed k-mode clustering and regression models to explore socioeconomic influences on suicide rates
- Led the team in data analysis and presentation delivery.