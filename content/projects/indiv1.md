+++
title = "Data Ethics on Predictive Policing Model Fairness"
description = "https://github.com/zaxiang/Implicit_Bias_Detection"
+++

<br>

# Project Overview:

Conducted independent research that studies whether unsupervised predictive policing machine learning algorithms can reveal biases in policing practice.

Implemented an improved algorithm based on k-means to cluster data to detect any implicit bias pattern
