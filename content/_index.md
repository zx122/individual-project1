+++
title = "Home Page"
template = "index.html"
+++

# About Myself

## Education

**University of California, San Diego**\
Bachelor of Science in Data Science (Minor in Philosophy) [2019-2023]

**Duke University**\
Master of Science in Electrical and Computer Engineering [2023-2025]

## Core Tech Stack

- **Programming Languages**: C, C++, Python, Java, JavaScript, SQL, R, MATLAB, Rust
- **Tools/Frameworks**: Verilog, PyTorch, PySpark, HTML/CSS, Linux, Emacs, GitHub, JUnit, AWS

## Languages

English (fluent), Mandarin (native), Japanese (basic)
